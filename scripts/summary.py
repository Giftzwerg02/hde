#!/usr/bin/env python3

import sys
import os


def create_file(path):
    if not os.path.exists(path):
        with open(path, 'w'): pass

        
def indentation_level(path):
    level = -root_path.count("/") - 1
    head, tail = os.path.split(path)
    while head:
        level += 1
        head, _ = os.path.split(head)
    return level


def read_header(path):
    with open(path, "r") as file: 
        header = file.readline().replace("#", "").strip()

        if len(header.strip()) == 0:
            _, name = os.path.split(path)
            header = name.replace(".md", "").strip()

        return header


def without_first_dir(path):
    return path[len(root_path) + 1:]


root_path = sys.argv[1]
summary_path = os.path.join(root_path, "SUMMARY.md")

with open(summary_path, "w") as summary:
    summary.write("# Summary\n\n")
    for current, _, files in os.walk(root_path):
        if current == root_path:
            continue
    
        readme = os.path.join(current, "README.md")
        create_file(readme)
        readme = without_first_dir(readme)

        indent = indentation_level(current) * 4
        
        _, name = os.path.split(current)
        summary.write(f"{indent * ' '}* [{name}]({readme})\n")
        
        indent += 4
        for file in files:
            if file.endswith(".md") and file not in ["README.md", "SUMMARY.md"]:
                file = os.path.join(current, file)
                name = read_header(file)
                file = without_first_dir(file)

                summary.write(f"{indent * ' '}* [{name}]({file})\n")
    
